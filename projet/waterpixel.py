from scipy import ndimage as ndi
import matplotlib.pyplot as plt
import numpy as np
import skimage.morphology as mp
from skimage.segmentation import watershed
import skimage
import sys
from skimage.segmentation import mark_boundaries
import math


def morphologicalGradient(img):
    dil = mp.dilation(img,mp.disk(3))
    ero = mp.erosion(img,mp.disk(3))
    return dil - ero

def get_marker(img,sigma):
    p = 2/3
    real_cell_radius= int(p*sigma/2)
    img = morphologicalGradient(img)

   # img = np.add((img<=255)*img,(img>255)*255).astype(np.uint8)
    marker = np.zeros((img.shape[0],img.shape[1])).astype(np.uint8)
    for i in range(int(img.shape[0]/sigma)+1):

        for j in range(int(img.shape[1]/sigma)+1):
            
            cell = img[int(max(i*sigma+sigma/2-real_cell_radius,0)):int(min(i*sigma+sigma/2+real_cell_radius,img.shape[0]-1)), int(max(j*sigma+sigma/2-real_cell_radius,0)):int(min(j*sigma+sigma/2+real_cell_radius,img.shape[1]-1))]
            if cell.shape[0] == 0 or cell.shape[1]==0:
                break
            minvalue = cell.min()
            mask = (cell == minvalue)
            cell[mask]= 1.0
            cell[~mask] = 0.0
            a = skimage.measure.label(cell, connectivity=1)
            a = a.astype(np.uint8)

            histo = skimage.exposure.histogram(a)
            if histo[0].shape[0] ==1 :
                cell[min(real_cell_radius,cell.shape[0]-1),min(real_cell_radius,cell.shape[1]-1)] = 1.0
            else:
                idx = np.argmax(histo[0][1:])
                mask2 = (a == idx+1 )
                cell[mask2]= 1.0
                cell[~mask2] = 0.0
            for i2 in range(cell.shape[0]):
                for j2 in range(cell.shape[1]):
                    if cell[i2,j2] == 1.0:
                        marker[int(i*sigma+i2+sigma/2-real_cell_radius),int(j*sigma+j2+sigma/2-real_cell_radius)] =255
    return marker


def getgrid(img,sigma):
    grid = np.zeros((img.shape[0],img.shape[1]))
    for i in  range(grid.shape[0]):
      for j in  range(grid.shape[1]):
          if i%sigma == 0 or j%sigma == 0:
              grid[i,j] = 1.0
    return grid


def distance(img,sigma,distancetype):
    d = np.zeros((img.shape[0],img.shape[1]))
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if distancetype == "euclid":
                d[i,j] =math.sqrt(math.pow((i%sigma)-sigma/2,2)+math.pow((j%sigma)-sigma/2,2))
            elif distancetype == "max":
                d[i,j] = max(abs((i%sigma)-sigma/2),abs((j%sigma)-sigma/2))
    return d

def regularized_gradient(img,k,sigma,distancetype ):
    g = morphologicalGradient(img.copy())
    g = g.astype(np.float64)
    coef = 8 * k/sigma
    d = distance(img,sigma,distancetype)
    res = g + coef * d
    return res

def color(img,label):
    label =label.astype(np.int64)
    for a in range(np.max(label)):
        mask = (label == a+1)
        total = np.sum(mask)
        mean_color = [0,0,0]
        tmpimg = img[mask]
        mean_color = np.sum(tmpimg,axis=0) 
        img[mask] = [mean_color[0]/total,mean_color[1]/total,mean_color[2]/total]

    return img

def waterpixel(filename,sigma,k,distancetype = "max"):
    
    image = skimage.io.imread(filename,as_gray = True)
    if np.issubdtype(image.dtype, np.float64):
        image = (image*255)
    imagergb = skimage.io.imread(filename)
    if np.issubdtype(imagergb.dtype, np.float64):
        imagergb = (imagergb*255)

    open = mp.area_opening(image, sigma*sigma/16)
    denoised = mp.area_closing(open, sigma*sigma/16)


    markers = markersshow = get_marker(denoised,sigma)

    markers = ndi.label(markers)[0]

    gradient = morphologicalGradient(image)

    # local gradient (disk(2) is used to keep edges thin)
    gradientreg =  regularized_gradient(denoised,k,sigma,distancetype)

    # process the watershed
    labels = watershed(gradientreg, markers)

    grid = getgrid(image,sigma)
    return gradient,imagergb,grid,gradientreg,labels,mark_boundaries(imagergb, labels,color=(255,255,255)),255-markersshow


if sys.argv[2]=="0":
    if len(sys.argv) == 4 :
        gradient,imagergb,grid,gradientreg,_,labels,markers = waterpixel(sys.argv[1],50,10,distancetype = sys.argv[3])
        _,_,_,_,_,labels1,_ = waterpixel(sys.argv[1],23, 0,distancetype = sys.argv[3])
        _,_,_,_,_,labels2,_ = waterpixel(sys.argv[1],23, 4,distancetype = sys.argv[3])
        _,_,_,_,_,labels3,_ = waterpixel(sys.argv[1],23, 10,distancetype = sys.argv[3])
    else:
        gradient,imagergb,grid,gradientreg,_,labels,markers = waterpixel(sys.argv[1],50,10)
        _,_,_,_,_,labels1,_ = waterpixel(sys.argv[1],23, 0)
        _,_,_,_,_,labels2,_ = waterpixel(sys.argv[1],23, 4)
        _,_,_,_,_,labels3,_ = waterpixel(sys.argv[1],23, 10)

    # display results
    fig, axes = plt.subplots(nrows=3, ncols=3, figsize=(8, 8),
                            sharex=True, sharey=True)
    ax = axes.ravel()




    ax[0].imshow(gradient, cmap=plt.cm.gray)
    ax[0].set_title("gradient")

    ax[1].imshow(imagergb)
    ax[1].set_title("Original")

    ax[2].imshow(grid, cmap = plt.cm.gray)
    ax[2].set_title("grid")

    ax[3].imshow(gradientreg, cmap=plt.cm.gray)
    ax[3].set_title("Gradient regularized")


    ax[4].imshow(labels)
    ax[4].set_title("Segmented")

    ax[5].imshow(markers, cmap=plt.cm.gray)
    ax[5].set_title("Markers")



    ax[6].imshow(labels1)
    ax[6].set_title("labels1")

    

    ax[7].imshow(labels2)
    ax[7].set_title("labels2")


    ax[8].imshow(labels3)
    ax[8].set_title("labels3")

    for a in ax:
        a.axis('off')

    fig.tight_layout()
    name = "result/"+sys.argv[1][0:sys.argv[1].find('.')]+"_"
    if len(sys.argv) == 4 :
        name +=sys.argv[3]+".png"
    else:
        name +="max"+".png"
    plt.savefig(name)
elif sys.argv[2]=="1":
    if len(sys.argv) == 6 :
        _,imagergb,_,_,labels,_,_ = waterpixel(sys.argv[1],int(sys.argv[3]),float(sys.argv[4]),distancetype = sys.argv[5])
    else:
        _,imagergb,_,_,labels,_,_ = waterpixel(sys.argv[1],int(sys.argv[3]),float(sys.argv[4]))
    
    img = color(imagergb,labels)
    name = "result/"+sys.argv[1][0:sys.argv[1].find('.')]+"_color_s_"+sys.argv[3]+"_k_"+sys.argv[4]+"_"
    if len(sys.argv) == 6 :
        name += sys.argv[5]+".png"
    else:
        name +="max"+".png"
    skimage.io.imsave(name,img)
    plt.imshow(img)  
    plt.show()
elif sys.argv[2]=="2":
    if len(sys.argv) == 6 :
        _,imagergb,_,_,labels,bounds,_ = waterpixel(sys.argv[1],int(sys.argv[3]),float(sys.argv[4]),distancetype = sys.argv[5])
    else:
        _,imagergb,_,_,labels,bounds,_ = waterpixel(sys.argv[1],int(sys.argv[3]),float(sys.argv[4]))
    
    img = color(imagergb,labels)
    mask = (bounds == [255,255,255])
    img[mask] = 0
    name = "result/"+sys.argv[1][0:sys.argv[1].find('.')]+"_bounded_s_"+sys.argv[3]+"_k_"+sys.argv[4]+"_"
    if len(sys.argv) == 6 :
        name += sys.argv[5]+".png"
    else:
        name +="max"+".png"
    skimage.io.imsave(name,img)
    plt.imshow(img)  
    plt.show()

