# Waterpixel based on watershed segmentation

Implémentation de waterpixel en python

## Prérequis

- Skimage
- Numpy
- scipy
- matplotlib

## Utilisation

Chacune de ces fonctions sauvegardent une image dans le dossier result. 

L'option distance type utilise soit :
 - max
 - euclid

Cette option permet de faire le choix entre une distance euclidienne ou le max des deux coordonnées. 
### Option 1

```bash
python filename 0 [distance type]
```

Crée une image permettant de comprendre le déroulé de l'algorithme avec quelques exemples.
(sigma =50 et k = 10 pour le premièr 
 exemple). Sigma représente la taille de chaque cellule de la grille et k la "force" de correspondance à la grille, plus k est grand plus les superpixels seront ressemblants à la grille.
Pour la dernière ligne sigma = 23 et k =  0 , 4 et 10)
<img src="result/22013_euclid.png" width="400">

### Option 2

```bash
python filename 1 sigma k [distance type]
```




Crée une image coloriant les superpixels avec sigma et k donné.

<img src="result/100075_color_s_8_k_0_euclid.png" width="400">

### Option 3

```bash
python filename 2 sigma k [distance type]
```

Crée une image coloriant les superpixels avec sigma et k donné, les superpixels sont délimités par une ligne noire.
<img src="result/153077_bounded_s_8_k_0_euclid.png
" width="400">

## Développement 

Les images utilisées pour les tests sont celles de la Berkeley segmentation database celle utilisée dans le papier.

<img src="result/108041_euclid.png" width="400">

Le résultat ci-dessus s'obtient en plusieurs étapes.

La première étape est de calculer le gradient morphologique de l'image (dilatation - erosion de l'image).
Une grille réguliére avec des carré de sigma pixels de large est ensuite créée (En l'occurence ce sont des carrés mais cela peut être des hexagones). Le gradient régularisé est ensuite calculé en additionnant au gradient morphologique une nouvelle donnée. cette donnée est la distance du pixel courant avec le bord le plus proche de la grille. Cette distance avant d'être additionnée est multipliée par un coefficient, dans l'article le coefficient est ((2 * k)/sigma) mais n'ayant pas des résultats cohérents avec la même image j'ai multiplié ce coefficient par 4  ((8 * k)/sigma). 
<img src="result/176035_max.png" width="400">

Pour cela j'ai créé deux types de distance (maximum et euclidienne).

L'avant dernière étape pour avoir l'image finale est de créer les marqueurs. A chaque case de la grille est associé un marqueur qui correspond au minimum de l'homothétie de celle-ci. Pour cela j'ai utilisé des masques  pour simplifier et rendre le résultat pllus rapide à produire. Ensuite si il existe plusieurs composantes connexes la plus grande est choisi. La zone la plus grande est déduite en labelisant chaque composante connexe à l'aide de la fonction skimage.measure.label ensuite en calculant la somme de chaque pixel ayant le même label (il faut le ramener à un auparavant), on peut calculer leur surface. Ensuite la fonction watershed est appliqué, celle-ci labelise chaque pixel en fonction du marqueur et du gradient. pour avoir les bords du superpixel la fonction mark_boundaries est utilisée.

## Conclusion
 Même si les résultats sont ressemblants avec les distances différentes nous pouvons apercevoir quelques changements, ici au niveau du nez du deuxième ours.
<img src="result/100075_bounded_s_16_k_4_euclid.png" width="400">
<img src="result/100075_bounded_s_16_k_4_max.png" width="400">

Cet algorithme permet d'avoir une certaine délimitation des objets (notamment visible sur le plongeur et les ours).

###  Côté artistique
Cet algorithme permet de créer des images avec un style particulier, cet effet de mosaïque/peinture grossière.